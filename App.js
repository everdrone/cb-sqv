import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

import SquircleView from './SquircleView';

// implement squircle
export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>SquircleView Demo</Text>
        <View style={styles.comparison}>
          <Text style={styles.instructions}>&lt;View/&gt;</Text>
        </View>
        <SquircleView style={styles.comparison}>
          <Text style={styles.instructions}>&lt;SquircleView/&gt;</Text>
        </SquircleView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  comparison: {
    width: 200,
    height: 200,
    margin: 30,
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    /* style props */
    backgroundColor: '#fff',
    shadowColor: '#120088',
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowOffset: {
      width: 0,
      height: 10,
    },
    borderRadius: 60,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
