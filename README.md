
# react-native-squircle-view

## Getting started

`$ npm install react-native-squircle-view --save`

### Mostly automatic installation

`$ react-native link react-native-squircle-view`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-squircle-view` and add `RNSquircleView.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNSquircleView.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<


## Usage
```javascript
import RNSquircleView from 'react-native-squircle-view';

// TODO: What to do with the module?
RNSquircleView;
```
  